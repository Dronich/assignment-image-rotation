#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

char* map_read_status_to_message(enum read_status status) {
    switch (status) {
        case READ_INVALID_BITS: return "Unable to read file";
        case READ_UNSUPPORTED_DEPTH: return "Unsupported color depth";
        case READ_INVALID_HEADER: return "Invalid header";
        case READ_INVALID_FILE: return "Invalid file format";
        case READ_INVALID_SIGNATURE: return "File doesn't exists";
        case READ_OK: return "File was read";
        case READ_FILE_NOT_FOUND: return "File not found";
        default: return "Unknown error";
    }
}

char* map_write_status_to_message(enum write_status status) {
    switch (status) {
        case WRITE_ERROR: return "Error occurred during saving file";
        case WRITE_OK: return "File saved successfully";
        default: return "Unknown error";
    }
}
