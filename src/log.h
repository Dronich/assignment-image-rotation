//
// Created by takem on 17.01.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_LOG_H
#define ASSIGNMENT_IMAGE_ROTATION_LOG_H
#include <stdio.h>

void print_info(char* msg) {
    fprintf(stdout, "%s\n", msg);
}

void print_error(char* msg) {
    fprintf(stderr, "%s\n", msg);
}

#endif //ASSIGNMENT_IMAGE_ROTATION_LOG_H
