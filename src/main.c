#include <stdio.h>
#include <string.h>

#include "util.h"
#include "rotate.h"
#include "log.h"

int main( int argc, char** argv ) {
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    struct image img;
    enum read_status read_status = read_image_from_file(argv[1], &img);
    if (read_status == READ_OK) {
        print_info(map_read_status_to_message(read_status));
        struct image new_image = rotate(img, CLOCKWISE);
        strtok(argv[1], ".")
        enum write_status write_status = write_image_to_file("rotated.bmp", &new_image);
        if (write_status == WRITE_OK) {
            print_info(map_write_status_to_message(write_status));
        } else {
            print_error(map_write_status_to_message(write_status));
        }
    } else {
        print_error(map_read_status_to_message(read_status));
    }
    return 0;
}
