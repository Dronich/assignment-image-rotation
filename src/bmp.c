#include "bmp.h"
#include "status.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>


bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

uint8_t get_padding(const uint32_t width) {
    size_t width_size = width * sizeof(struct pixel);
    if (width_size % PADDING_SIZE == 0) return 0;
    return PADDING_SIZE - (width_size % PADDING_SIZE);

}

struct bmp_header get_header_from_image(const struct image* img) {
    uint32_t headerSize = sizeof(struct bmp_header);
    uint32_t pixelSize = sizeof(struct pixel);

    struct bmp_header h = (struct bmp_header){
            .bfType = BMP_TYPE,
            .bfileSize = img->width * img->height + headerSize + img->width % 4 * img->height,
            .bfReserved = 0,
            .bOffBits = headerSize,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = COLOR_DEPTH,
            .biCompression = 0,
            .biSizeImage = img->width * img->height * pixelSize,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    return h;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    uint32_t header_size = sizeof (struct bmp_header);
    uint32_t pixel_size = sizeof (struct pixel);

    struct bmp_header h = get_header_from_image(img);
    if (!fwrite(&h, header_size, 1, out)) return WRITE_ERROR;

    char paddingBytes[3] = { 0 };
    uint32_t padding = get_padding(img->width);

    for (size_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, pixel_size, img->width, out) != img->width) return WRITE_ERROR;
        if (fwrite(paddingBytes, padding, 1, out) != 1 && padding != 0) return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header h = { 0 };
    uint8_t pixel_size = sizeof (struct pixel);
    if (!read_header(in, &h)) return READ_INVALID_HEADER;
    if (h.biBitCount != COLOR_DEPTH) return READ_UNSUPPORTED_DEPTH;
    if (h.bfType != BMP_TYPE) return READ_INVALID_SIGNATURE;
    if (fseek(in, h.bOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;

    img->width = h.biWidth;
    img->height = h.biHeight;
    img->data = malloc(h.biWidth * h.biHeight * sizeof (struct pixel));

    const uint8_t padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, sizeof (struct pixel), img->width, in) < pixel_size) return READ_INVALID_BITS;
        if (fseek(in, padding, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }
    return READ_OK;
}




