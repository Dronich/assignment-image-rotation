//
// Created by takem on 17.01.2021.
//
#include <malloc.h>
#include "image.h"
#include "bmp.h"

enum write_status write_image_to_file(const char* filename, struct image const* img) {
    if (!filename) return WRITE_ERROR;
    FILE* f = fopen(filename, "wb");
    if (!f) return WRITE_ERROR;
    return to_bmp(f, img);
}

enum read_status read_image_from_file( const char* filename, struct image* img ) {
    if (!filename) return READ_INVALID_SIGNATURE;
    FILE* f = fopen(filename, "rb");
    if (!f) return READ_FILE_NOT_FOUND;
    enum read_status status = from_bmp(f, img);
    fclose(f);
    return status;
}