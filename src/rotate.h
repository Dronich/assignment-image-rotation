//
// Created by takem on 17.01.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "image.h"

enum direction {
    CLOCKWISE = 0,
    COUNTER_CLOCKWISE
};

struct image rotate(struct image img, enum direction dir);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
