//
// Created by takem on 17.01.2021.
//

#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdio.h>
#include "inttypes.h"

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum write_status write_image_to_file(const char* filename, struct image const* img);
enum read_status read_image_from_file( const char* filename, struct image* img );
#endif