#ifndef _UTIL_H_
#define _UTIL_H_

#include "image.h"
#include "status.h"

_Noreturn void err( const char* msg, ... );

char* map_read_status_to_message(enum read_status status);
char* map_write_status_to_message(enum write_status status);

#endif
