//
// Created by takem on 17.01.2021.
//
#include "rotate.h"
#include <malloc.h>

struct image rotate(struct image img, enum direction dir) {
    struct image new_img = {
            .height = img.height,
            .width = img.width,
            .data = malloc( img.width * img.height * sizeof (struct pixel))
    };

    for (int x = 0; x < img.width; ++x) {
        for (int y = 0; y < img.height; ++y) {
            switch (dir) {
                case CLOCKWISE:
                    *(new_img.data + new_img.width * x + y) = *(img.data + y * img.width + x);
                case COUNTER_CLOCKWISE:
                    *(new_img.data + new_img.width * x + y) = *(img.data + (img.height - y) * img.width + x);
            }
        }
    }

    return new_img;
}
